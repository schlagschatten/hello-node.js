const express = require("express"),
      server = express(),
      port = 3000,
      host = 'localhost',

      db = require('./database'),
      jsonParser = express.json(),
      cookieParser = require('cookie-parser'), //для чтения cookie, отправленного от клиента

      path = require('path'),
      user = require('mongodb');

server.use(cookieParser('MY SECRET'));

//Remove Cookies
// server.post('/removeCookies',function(request, response){
//     response.clearCookie('cookieName');
//     response.send("Cookie has been cleared");
// });

server.post('/form/creatUser', jsonParser, function (request, response) {
    console.log(request);
    console.log(request.body);
    if(!request.body) return response.sendStatus(400);
    const userName = request.body.name;
    const userLogin = request.body.login;
    const userPassword = request.body.password;
    const userRepeatPassword = request.body.repeatPassword;
    const userEmail = request.body.email;

    db.saveUser(userName, userLogin, userPassword, userRepeatPassword, userEmail);
    response.json(request.body);
});

server.post('/form/checkLoginUser', jsonParser, function (request, response){
    console.log(request);
    console.log(request.body);

    console.log('Cookie: ', request.cookies);
    let options = {
        maxAge: 1000 * 60 * 15,
        httpOnly: true, // Файл cookie доступен только веб-серверу
        signed: true // Указывает, должен ли быть подписан файл cookie
    }

    response.cookie('cookieName', 'cookieValue', options);


    if(!request.body) return response.sendStatus(400);
    const userLogin = request.body.login;
    const userPassword = request.body.password;


    //подключились - нашли юзера
    function onSuccess(user){
        console.log('есть в базе');
        response.send({
            redirect:'/main',
        });
    }

    // юзера нету в базе
     function onSuccessNotUser(user){
         console.log('юзера нету в базе')
         response.send({
             notUserMessage: "Нету юзера в базе, зарегестрируйтесь"
         });

     }

    //ошибка подключения к бд
    function onError(){
        console.log('error');
        response.send({
            errorMessage: "Нету подключения к базе",
        });
    }

    db.checkUser(userLogin, userPassword, onError, onSuccess, onSuccessNotUser);
})


server.use(express.static(__dirname + '/../webapp/public'));

server.use('/sign/bar', function (request, response) {
    console.log(__dirname);
    console.log(__dirname + '/../webapp/public/sign/bar/sign.html');
    response.sendFile(path.resolve('src/webapp/public/sign/bar/sign.html'));

    //response.redirect('registration');
});
 server.use('/sign/registration', function(request, response){
     response.sendFile(path.resolve('src/webapp/public/sign/registration/registration.html'));
 });

server.use('/main', function(request, response){
    response.sendFile(path.resolve('src/webapp/public/main.html'));
});

server.listen(port, host, function () {
    console.log(`Server listens http://${host}:${port}`);
});





