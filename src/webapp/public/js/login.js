$(function(){
    $('#button_user_check').click(function() {
        let json = {
            login: document.getElementById('num2').value,
            password: document.getElementById('num3').value
        };
        $.ajax({
            url: ("/form/checkLoginUser"),
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            processData: false,
            data: JSON.stringify(json),
            success: function (data) {
                let result = data;
                console.log(result);
                if(data.redirect){
                    location.href = data.redirect;
                }
                if(data.errorMessage){
                   let message = data.errorMessage;
                    alert(message);
                }
                if(data.notUserMessage){
                    let message = data.notUserMessage;
                    alert(message)
                }

            },
            error: function (error, status, strErr) {
                alert(strErr);
            }
        });
        return false;
    })
    });

// var options = {host:url_parsed.host,
//     path:url_parsed.path,
//     method:'GET',
//     headers:{'Cookie':cookie},
//     'Accept':'/',
//     'Connection':'keep-alive', };